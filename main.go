package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path"
	"strings"
)

type SBOM struct {
	Repo         string `json:"repo"`
	Organisation string `json:"organisation"`
	SBOM         any    `json:"sbom"`
}

func queryDependencyData(org, repo string) (*SBOM, error) {
	req, err := http.NewRequest(http.MethodGet, fmt.Sprintf("https://api.github.com/repos/%s/%s/dependency-graph/sbom", org, repo), nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Authorization", "Bearer "+os.Getenv("GITHUB_TOKEN"))
	req.Header.Add("Accept", "application/vnd.github+json")
	req.Header.Add("X-GitHub-Api-Version", "2022-11-28")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("failed to retrieve SBOM for %s/%s: HTTP %d", org, repo, resp.StatusCode)
	}

	var sbom SBOM
	defer resp.Body.Close()

	data, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read response body for %s/%s: %v", org, repo, err)
	}

	err = json.Unmarshal(data, &sbom)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal response body as JSON for %s/%s: %v", org, repo, err)
	}

	sbom.Repo = repo
	sbom.Organisation = org

	return &sbom, nil
}

func main() {
	if os.Getenv("GITHUB_TOKEN") == "" {
		log.Fatal("A GITHUB_TOKEN environment variable must be set to a Personal Access Token (Classic)")
	}

	if len(os.Args) == 1 {
		log.Fatal("Usage: ./dependabot-graph jamietanna/jamietanna deepmap/oapi-codegen ...")
	}

	for _, arg := range os.Args[1:] {
		parts := strings.SplitN(arg, "/", 2)
		org := parts[0]
		repo := parts[1]
		sbom, err := queryDependencyData(org, repo)
		if err != nil {
			log.Printf("Error retrieving dependency data for %s/%s: %v", org, repo, err)
			continue
		}

		data, err := json.Marshal(sbom)
		if err != nil {
			log.Printf("Error marshalling SBOM as JSON for %s/%s: %v", org, repo, err)
			continue
		}
		data = append(data, '\n')

		// make sure that the `out` folder exists
		os.MkdirAll("out", os.ModePerm)
		os.WriteFile(path.Join("out", fmt.Sprintf("%s-%s.json", org, repo)), data, 0644)
		log.Printf("Successfully retrieved dependency data for %s/%s", org, repo)
	}
}
