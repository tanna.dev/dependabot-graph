# Dependabot dependency graph exporter

A tool to export the dependency graph from [GitHub Advanced Security's Dependency Graph](https://docs.github.com/en/code-security/supply-chain-security/understanding-your-software-supply-chain/about-the-dependency-graph) and allows a straightforward means to discover your dependency tree.

This is inspired by code from [_Analysing our dependency trees to determine where we should send Open Source contributions for Hacktoberfest_](https://deliveroo.engineering/2022/09/29/hacktoberfest-dependency-analysis.html) and modified to work within the world of [dependency-management-data](https://dmd.tanna.dev/).

As of [v0.2.0](https://gitlab.com/tanna.dev/dependabot-graph/-/tags/v0.2.0), this uses the [Software Bill Of Materials (SBOM) endpoint](https://docs.github.com/en/rest/dependency-graph/sboms?apiVersion=2022-11-28) to generate the dependency graph. If you require the old style response, consider pinning to a previous version of the tool.

## Installation

```sh
go install gitlab.com/tanna.dev/dependabot-graph@latest
```

## Usage

If you wanted to get dependency data for the repos `jamietanna/jamietanna` and `deepmap/oapi-codegen` you would run it like so:

```sh
env GITHUB_TOKEN=ghp_... dependabot-graph jamietanna/jamietanna deepmap/oapi-codegen
```

This then creates files i.e.

```
out/jamietanna-jamietanna.json
out/deepmap-oapi-codegen.json
```

**Note** that this requires a GitHub Personal Access Token, which can be a Fine-Grained token with _read-only_ access.

## License

This code is licensed under Apache-2.0.
